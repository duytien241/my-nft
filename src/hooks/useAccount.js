import { queryClient } from "../App";
import { useCallback } from "react";
import { useQuery, useQueryClient } from "react-query";

export default function useAccount() {
  const queryClient = useQueryClient();
  const { data: account } = useQuery("account", () =>
    queryClient.getQueryData("account")
  );
  const setAccount = useCallback(
    (account) => queryClient.setQueryData("account", account),
    [queryClient]
  );

  return { account: account, setAccount };
}
export function getAccount() {
  return Number(queryClient.getQueryData("account"));
}
