import React from "react";
import "./FeaturedHeader.css";

const FeaturedHeader = () => {
  return (
    <div className="featured-header">
      <h3>Danh sách sản phẩm nổi bật</h3>
      <p>Sân phẩm nổi bật.</p>
    </div>
  );
};

export default FeaturedHeader;
