import React from "react";
import { PrimaryButton } from "../uiComponents/buttons";
import "./AboutDescription.css";

const AboutDescription = () => {
  return (
    <div className="about-description" id="mynft">
      <h3>NFT Phổ biến</h3>
      <PrimaryButton title="Show more" />
    </div>
  );
};

export default AboutDescription;
