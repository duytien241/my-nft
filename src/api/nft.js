import { sendGet } from "./axios";

export const getListNft = (idolId, payload) => sendGet(`/nft/list`, payload);
