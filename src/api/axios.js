import Axios from "axios";

const axiosInstance = Axios.create({
  timeout: 3 * 60 * 1000,
  baseURL: "https://nft.recruit-api.site/",
});

export const sendGet = (url, params) =>
  axiosInstance.get(url, { params }).then((res) => res.data);
export const sendPost = (url, params) =>
  axiosInstance.post(url, params).then((res) => res.data);
export const sendPut = (url, params) =>
  axiosInstance.put(url, params).then((res) => res.data);
