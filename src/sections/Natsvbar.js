import React, { useEffect, useState } from "react";
import MenuToggler from "../components/navbarComponents/MenuToggler";
import NavLists from "../components/navbarComponents/NavLists";
import NavSearch from "../components/navbarComponents/NavSearch";
import { PrimaryButton } from "../components/uiComponents/buttons";
import searchIcon from "../images/navbarImages/searchIcon.svg";
import "./Navbar.css";
import Web3 from "web3";
import useAccount from "../hooks/useAccount";

const Navbar = () => {
  const [isactive, setIsactive] = useState(false);
  const [isConnected, setIsConnected] = useState(false);
  const [address, setAddress] = useState("");
  const { setAccount } = useAccount();

  useEffect(() => {
    const checkConnection = async () => {
      if (window.ethereum) {
        try {
          await window.ethereum.request({ method: "eth_requestAccounts" });
          setAddress(window.ethereum.selectedAddress);
          setAccount(window.ethereum.selectedAddress);
          setIsConnected(true);
        } catch (error) {
          console.log(error);
        }
      }
    };
    checkConnection();
  }, []);

  const toggleActive = () => {
    setIsactive(!isactive);
    if (!isactive) {
      var xPos = window.scrollX;
      var yPos = window.scrollY;
      window.onscroll = () => {
        window.scroll(xPos, yPos);
      };
    } else {
      window.onscroll = "";
    }
  };

  const connectMetamask = () => {
    var web3;
    if (typeof window.ethereum !== "undefined") {
      // Nếu đã kích hoạt, sử dụng nhà cung cấp của MetaMask
      web3 = new Web3(window.ethereum);
      // Yêu cầu người dùng cho phép trang web truy cập vào tài khoản MetaMask
      window.ethereum.enable().catch(function (error) {
        // Xử lý lỗi nếu người dùng từ chối cho phép
        console.error("Kết nối MetaMask thất bại:", error);
      });
    } else {
      // Nếu chưa kích hoạt, hiển thị thông báo lỗi
      console.error("MetaMask chưa được kích hoạt trong trình duyệt.");
    }
    web3 = new Web3(
      new Web3.providers.HttpProvider(
        "https://data-seed-prebsc-1-s1.binance.org:8545"
      )
    );

    // Sử dụng đối tượng web3 để gọi các hàm contract, giao dịch, etc. trên BSC Testnet
    // Ví dụ:
    web3.eth
      .getAccounts()
      .then(function (accounts) {
        // Lấy danh sách tài khoản MetaMask đang kết nối
        console.log("Danh sách tài khoản MetaMask:", accounts);
        setIsConnected(true);
        setAccount(window.ethereum.selectedAddress);
      })
      .catch(function (error) {
        console.error("Lỗi:", error);
      });
  };

  const disconnectWallet = () => {
    // Kiểm tra xem trình duyệt của người dùng có hỗ trợ Web3 hay không
    if (typeof window.ethereum !== "undefined") {
      // Nếu hỗ trợ, sử dụng nhà cung cấp của MetaMask
      var web3 = new Web3(window.ethereum);

      // Yêu cầu người dùng cho phép trang web truy cập vào tài khoản MetaMask
      window.ethereum
        .enable()
        .then(function () {
          // Đã kết nối thành công, có thể sử dụng web3 để gọi các hàm contract, giao dịch, etc.
          console.log("Đã kết nối MetaMask thành công!");

          // ... Các mã lệnh xử lý sau khi kết nối thành công ...

          // Để ngắt kết nối với MetaMask, xóa đối tượng web3 hiện tại
          web3 = null;
          console.log("Đã ngắt kết nối MetaMask!");
          setIsConnected(false);
        })
        .catch(function (error) {
          // Xử lý lỗi nếu người dùng từ chối cho phép
          console.error("Kết nối MetaMask thất bại:", error);
        });
    } else {
      // Nếu không hỗ trợ, hiển thị thông báo lỗi
      console.error("Trình duyệt của bạn không hỗ trợ MetaMask.");
    }
  };
  return (
    <nav>
      <div className="nav-inner">
        <div className="nav-logo">
          <h2>
            Demo<span>NFT</span>
          </h2>
        </div>
        <MenuToggler
          className={`menu-toggler ${isactive ? "active" : ""}`}
          onClick={toggleActive}
        />
        <div className="nav-search">
          <NavSearch src={searchIcon} alt="Nav Search Icon" />
        </div>
        <div className={`nav-links ${isactive ? "active" : null}`}>
          <NavLists linkName="My NFT" linkPath="mynft" />
          <NavLists linkName="Feature" linkPath="featured" />
          {isConnected ? (
            <>
              <p>{address?.slice(0, 10)}....</p>
              <button
                className="primaryButton"
                onClick={() => disconnectWallet()}
              >
                Disconnect
              </button>
            </>
          ) : (
            <button className="primaryButton" onClick={() => connectMetamask()}>
              Select Wallet
            </button>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
