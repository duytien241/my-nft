import React from "react";
import FeaturedHeader from "../components/featuredComponents/FeaturedHeader";
import { Card } from "../components/uiComponents/card";
import YellowPainting from "../images/featuredImages/nft-YellowPainting.png";
import DomeOnGradientBackground from "../images/featuredImages/nft-DomeOnGradientBackground.png";
import GradientBlob from "../images/featuredImages/nft-GradientBlob.png";
import "./Featured.css";
import contractABI from "../abi.json";
import Web3 from "web3";
import { useQuery } from "react-query";
import { getListNft } from "../api/nft";

const featuredData = [
  {
    id: 1,
    src: YellowPainting,
    alt: "DEMO NFT",
    user: "@Johny",
    name: "DEMO",
    bidStatus: "Current Bid",
    price: "0.005BNB",
    buttonText: "Buy",
    defaultTheme: true,
  },
  {
    id: 2,
    src: DomeOnGradientBackground,
    alt: "DEMO NFT 2",
    user: "@Jeniffer",
    name: "DDEMO NFT 2",
    bidStatus: "Current Bid",
    price: "0.007BNB",
    buttonText: "Buy",
    defaultTheme: true,
  },
  {
    id: 3,
    src: GradientBlob,
    alt: "DEMO NFT 3",
    user: "@Eric",
    name: "DEMO NFT 3",
    bidStatus: "Current Bid",
    price: "0.008BNB",
    buttonText: "Buy",
    defaultTheme: false,
  },
];
const contractAddress = "0x4c2A09f14d89359B62990753Ee83d1ce0F6e9800";
const web3 = new Web3("https://data-seed-prebsc-1-s1.binance.org:8545");
const contract = new web3.eth.Contract(contractABI, contractAddress);

const Featured = () => {
  // Khởi tạo đối tượng web3 với provider của testnet Ethereum (ví dụ: Ropsten)
  // Khởi tạo đối tượng web3 với provider của testnet BSC (ví dụ: Testnet Binance Smart Chain Testnet)

  const listNft = useQuery("list-nft", getListNft);

  const onClickButton = (address) => {
    web3.eth.net
      .isListening()
      .then(() => console.log("Đã kết nối thành công với BSC Testnet"))
      .catch((error) =>
        console.error("Không thể kết nối với BSC Testnet:", error)
      );
    var accounts = web3.eth.accounts;
    var currentAccount = accounts[0];
    console.log(currentAccount);
    contract.methods
      .buyNFT(address)
      .send({
        from: "0x60b6621ba14eeea5d6f226a9eba0136a0961f602",
        gas: "3000000",
      })
      .then((result) => {
        // Xử lý kết quả
        console.log(result);
      })
      .catch((error) => {
        // Xử lý lỗi
        console.log(error);
      });
  };
  return (
    <section id="featured">
      <div className="featured-inner">
        <FeaturedHeader />
        <div className="featured-cards">
          <div className="featured-cards_column">
            {listNft?.data?.map((item) => {
              return (
                <Card
                  key={item?.tokenId}
                  src={item?.normalizedMetadata?.image || GradientBlob}
                  alt={"DEMO NFT"}
                  user={"us0x6...1f602"}
                  name={`${item?.name}- #${item?.tokenId}`}
                  // bidStatus={bidStatus}
                  price={"0.1 BNB"}
                  buttonText={"Buy"}
                  defaultTheme={true}
                  onClick={onClickButton}
                  address={item?.tokenId}
                />
              );
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Featured;
