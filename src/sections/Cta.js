import React from "react";
import { SecondaryButtonAlt } from "../components/uiComponents/buttons";
import "./Cta.css";
import useAccount from "../hooks/useAccount";

const Cta = () => {
  const { account } = useAccount();
  return (
    <section id="cta">
      <div className="cta-inner">
        <div className="cta-details">
          <h2>My NFT</h2>
          {account ? (
            <SecondaryButtonAlt title="Không có dữ liệu" />
          ) : (
            <SecondaryButtonAlt title="Coming soon" />
          )}
        </div>
      </div>
    </section>
  );
};

export default Cta;
