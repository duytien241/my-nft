import React from "react";
import "./Preloader.css";

const Preloader = ({ displayNone }) => {
  return (
    <section id="preloader" className={`${!displayNone ? "active" : ""}`}>
      <div className="preloader-inner">
        <div className="preloader-credits">
          <h2>DemoNFT</h2>
          <h2>Project by Son Nguyen.</h2>
        </div>
      </div>
    </section>
  );
};

export default Preloader;
