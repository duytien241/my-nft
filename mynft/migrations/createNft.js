const MyNFT = artifacts.require("NFTMarketplace");

module.exports = async function (callback) {
  const instance = await MyNFT.deployed();
  await instance.mintNFT(
    "0xDf926CbD09C5e7be7e31dC4cFd0686532AD82366",
    "0x1234571121",
    "https://www.artnews.com/wp-content/uploads/2022/01/unnamed-2.png?w=631",
    4
  );
  console.log("NFT created with ID 2");
  callback();
};
