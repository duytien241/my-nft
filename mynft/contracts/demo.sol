// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract NFTMarketplace is ERC721 {
    address payable owner;
    uint256 public tokenId;
    uint256 public price;
    uint256[] private _tokenIds;
    // Mapping lưu trữ giá trị của mỗi NFT
    mapping(uint256 => uint256) private _tokenPrices;
    mapping(uint256 => string) private _tokenImages;
    mapping(uint256 => string) private _tokenName;

    
    constructor() ERC721("NFT Marketplace", "NFTM") {
        owner = payable(msg.sender);
    }
    
 // Hàm để tạo NFT mới
    function mintNFT(address to, uint256 tokenId, string memory tokenImage, uint256 price, string memory name) public {
        _mint(to, tokenId);
        _tokenIds.push(tokenId);
        _tokenPrices[tokenId] = price;
        _tokenImages[tokenId] = tokenImage;
        _tokenName[tokenId] = name;
    }
    
    function buyNFT(uint256 _tokenId) public payable {
        require(_exists(tokenId), "NFT not found");
        require(msg.value >= _tokenPrices[tokenId], "Insufficient value");
        address owner = ownerOf(tokenId);
        payable(owner).transfer(msg.value);
        _transfer(owner, msg.sender, tokenId);
        _tokenPrices[tokenId] = 0;
    }
    
    function withdrawFunds() public {
        require(msg.sender == owner, "Only owner can withdraw funds");
        owner.transfer(address(this).balance);
    }

    // Hàm để lấy giá trị của một NFT
    function getTokenPrice(uint256 tokenId) external view returns (uint256) {
        require(_exists(tokenId), "NFT not found");
        return _tokenPrices[tokenId];
    }

  // Hàm để lấy ảnh (hoặc URI) của một NFT
    function getTokenImage(uint256 tokenId) external view returns (string memory) {
        require(_exists(tokenId), "NFT not found");
        return _tokenImages[tokenId];
    }
}
